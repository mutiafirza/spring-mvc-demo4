<!DOCTYPE web-app PUBLIC "-//Sun Microsystems, Inc.//DTD Web Application 2.3//EN" "http://java.sun.com/dtd/web-app_2_3.dtd" >
<%@taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<%
	request.setAttribute("contextName",request.getContextPath());
%>
<html>
<body>
	<p>Welcome to Spring MVC Tutorial</p>
	
	<form action="${contextName}/mhs" method="post">
	<input type="hidden" name="mode" value="edit">
	<input type="hidden" name="id" value="${mahasiswa.id}">
	NIM <input type="text" name="nim" value="${mahasiswa.nim}"><br>
	Nama <input type="text" name="nama" value="${mahasiswa.nama}"><br>
	Alamat <input type="text" name="alamat" value="${mahasiswa.alamat}"><br>
	Tanggal Lahir <input type=date name="tanggal" value="${mahasiswa.tanggal}"><br>
	<button type="submit">Simpan</button>
	<p> data yang telah di simpan </p>
	</form>
	
	<ol>
	<c:forEach var="mhs" items="${mhslist}">
	<li>${mhs.nim} - ${mhs.nama} - ${mhs.tanggal} -  <a href="${contextName}/mhs/edit?id=${mhs.id}">Edit</a>
	<a href="${contextName}/mhs/delete?id=${mhs.id}">Delete</a>
	</li>
	
	</c:forEach>
	</ol>
</body>
</html>
